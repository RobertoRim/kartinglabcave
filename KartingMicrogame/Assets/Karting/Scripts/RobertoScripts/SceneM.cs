﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneM : MonoBehaviour
{
    // Start is called before the first frame update
    //Uso este script para gestionar los cambios de escenas
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void EmpezarElJuego()
    {
        Debug.Log("Empieza el juego");
        SceneManager.LoadScene(1);
        // Manager.numColeccionables = ;
    }
    public void Exit()
    {
        Debug.Log("Salir del juego");
        Application.Quit();
    }
    public void SalirAlMenu()
    {
        Debug.Log("Salgo al menu");
        SceneManager.LoadScene(0);
    }
}
