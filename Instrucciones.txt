Escenas añadidas o cambiadas:

Main Menu: 
-Nueva escena de inicio del juego, se añadieron un canvas con 2 botones y 2 textos pro.
-Luego por otra parte se añadio un objeto vacio SceneManager en el que se añadio el Track Manager para llevar hay la puntuación de la mejor vuelta y
un script creado por mi llamado Scene M, el cual sirve para gestionar los cambios de escenas.
-También se añadio un GameManager el cual sirve para gestionar los de mas datos que se puedan añadir en un futuro. 

SampleScene: 
-Se añadio un canvas con botones mapeados usando el imput jostick que ya estaba hecho. 
-El TrackManager con el bool reiniciar activo el record se reiniciaria cogiendo el record de record aux.
-Un CanvasGameover el cual es visible al terminar la carrera junto con 2 botones de volver a jugar o salir al menu.
-TimeDisplayCanvas se le añadio un texto pro de la cuenta de las monedas obtenidas dentro de la carrera (Este solo funciona con el bool totales en falso)
También se añadio un BestLap que solo sera visible momentaneamente cuando superes el record obtenido en cualquier carrera anterior.
-Un SceneManager para gestionar escenas.
-Monedas que se ven a lo largo de la carrera
-GameManager para gestionar la cuenta de las monedas.
-Al kartmovement se le puso en el apartado input el script Gamepad Input.

Controles:
-Son los mismos que venian por defecto.
(Flechas moverse, espacio saltas y si mantienes derrapas,R reinicias)
En movil (Jostick te mueves, A aceleras, B saltas, si mantienes derrapas, Y reinicias).

Scripts: (En la carpeta Karting/Scripts/y las mias son RobertoScripts)
Nota: Todos los Scripts que han sido modificados estan indicadas entre "//--------------"

.TrackManager:
-En el void start comparo si esta el bool reiniciar activo o no, si esta activo se borra el record guardado localmente anteriormente y se guarda lo escrito
en recordaux.(Mayormente para pruebas). si no esta activo no hace nada.
Continua guardando en la variable nuevorecord lo que habia guardado localmente. Luego compara que el record no sea null y si no es null, se muestra en pantalla.
-Uso la clase que ya habia y la utilizo para comparar si ha supero el mejor record existente, si lo supera se guarda ese nuevo record.Lo muestra en 
pantalla y llama a otra función para que desaparezca al cabo de un tiempo.

.SceneManager: Llamo a este script solo con botones de canvas y le digo a que escena tiene que cambiar.
.Coins: Cuando el jugador con tag Player choca contra la moneda (todas tienen este script). Esta llama al GameManager y empieza la funcion Coins,y esta
se destruye.
.GameManager: las monedas en las que si esta activado el bool Totales mostrara las monedas totales obtenidas a lo largo del conjunto de todas las carreras.
y si no solo las de la carrera.

