﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
using TMPro;
public class GameManager : MonoBehaviour
{
    public float auxmonedas;
    public float monedas;
    public float monedasTotales;
    public bool Totales;
    public TextMeshProUGUI ContadorMoneda;

    // Start is called before the first frame update
    void Start()
    {
        //En el Start cojo de playerprefs las monedas totales guardadas localmente. El bool es para elegir si se muestra en pantalla o no, por ejemplo en el menu si pero en la carrera no.
        monedasTotales = PlayerPrefs.GetFloat("Coins");
        if (Totales == true)
        {
            Debug.Log(monedasTotales);
            ContadorMoneda.text = "Total Coins: " + monedasTotales;
        }
            
       
    }

    // Update is called once per frame
    void Update()
    {
        //Aqui se muestra el numero de monedas que vas obteniendo en la carrera.
        if(Totales==false)
        ContadorMoneda.text = "Coins: " + monedas;
    }
    public void coins()
    {
        //Aqui uso un metodo auxiliar para facilitarme las cosas y guardo las monedas totales en playerprefs
        auxmonedas=+1;
        monedas= monedas + auxmonedas;
        monedasTotales = monedasTotales + auxmonedas;
        PlayerPrefs.SetFloat("Coins",monedasTotales);
        Debug.Log(monedas);
        Debug.Log(monedasTotales);
        
    }

}
