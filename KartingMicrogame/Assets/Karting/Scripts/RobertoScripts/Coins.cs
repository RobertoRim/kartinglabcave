﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coins : MonoBehaviour
{
    public GameObject GameManager;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter(Collider other)
        //Cuando una moneda da con un tag player esta llama al GameManager y le dice que empiece con la funcion coins y luego se destruya el propio gameObject.
    {
        if (other.gameObject.tag == "Player")
        {
            GameManager.GetComponent<GameManager>().coins();
            Destroy(gameObject);
        }
    }
}
